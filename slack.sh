#!/bin/bash
#
# Script to send a Slack message trigerred by certain event

webhookurl="$SLACK_TOKEN"
channel="hse-covid-vaccine-registration"
message="It is time to register if you were born on ${1}"
colour="#36a64f" # Green

short_msg (){
curl -X POST --data-urlencode 'payload={"username":"Gitlab", "icon_emoji": "microbe", "channel": "'${channel}'",
"attachments":[
      {
          "fallback": "HSE Covid Vaccine Registration Pipeline",
          "color": "'${colour}'",
          "title": "HSE Covid Vaccine Registration Pipeline",
          "fields": [
              {
                  "title": "Message",
                  "value": "'"${message}"'",
                  "short": true
              }
          ],
      }
   ]
}' \
  ${webhookurl}
}

short_msg
